package com.algaworks.algamoneyapi.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.algaworks.algamoneyapi.model.Categoria;
import com.algaworks.algamoneyapi.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Categoria atualizar(Long codigo, Categoria categoria) {
		Categoria categoriaSalva = buscarCategoriaPeloCodigo(codigo);
		BeanUtils.copyProperties(categoria, categoriaSalva, "codigo");
		categoriaRepository.save(categoriaSalva);
		
		
		return categoriaSalva;
	}

	private Categoria buscarCategoriaPeloCodigo(Long codigo) {
		Optional<Categoria> categoriaOpt = categoriaRepository.findById(codigo);
		
		if(!categoriaOpt.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		
		return categoriaOpt.get();
	}
	
}
